<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.marcus.ticustacus.Tictactoe" %>

<%
    
    String posiciones = request.getParameter("posiciones");
    if (posiciones != null && posiciones != ""){
        char[] pos = new char[9];
        pos[0] = posiciones.charAt(0);
        pos[1] = posiciones.charAt(1);
        pos[2] = posiciones.charAt(2);
        pos[3] = posiciones.charAt(3);
        pos[4] = posiciones.charAt(4);
        pos[5] = posiciones.charAt(5);
        pos[6] = posiciones.charAt(6);
        pos[7] = posiciones.charAt(7);
        pos[8] = posiciones.charAt(8);
    }
    
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css\styles.css">

    <title>Tic Tac Toe</title>
</head>
<body>
    <div class="centrat">

        <div class="row">

            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
        </div>

        <div class="row">

            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
            
        </div>

        <div class="row">
    
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
            <div class="container pos0">
                <img src="img\blank.png" class="img-fluid casilla jug0">
            </div>
        
        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>

        var equis = "img\\playX.png";
        var circulo = "img\\playO.png";
        var blank = "img\\blank.png";
        var jugador = 1;

        $(document).on("click",".casilla", function(){

            $(this).removeClass("jug0");
            $(this).addClass("jug"+jugador);

            if (jugador == 1){
                $(this).attr("src", equis);
                jugador = 2;
            }
            else if (jugador == 2){
                $(this).attr("src", circulo);
                jugador = 1;
            }
        });
    
    </script>
    
</body>
</html>