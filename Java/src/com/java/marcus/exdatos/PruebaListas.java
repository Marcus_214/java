package com.java.marcus.exdatos;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class PruebaListas {

    public static void main(String[] args) {
        
        List<String> ciudades = new ArrayList<String>();

        for (String ciudad : Datos.provincias()) {
            if (!ciudades.contains(ciudad)){
                ciudades.add(ciudad);
            }
        }
        
        Collections.sort(ciudades);

        System.out.println(ciudades);

    }

}