package com.java.marcus.pruebasmarc;

public class GeneradorTiradas {

    int num;
    int dado;

    GeneradorTiradas(int veces, int tipo_dado) {
        this.num = veces;
        this.dado = tipo_dado;

        for (int i = 0; i < num; i++) {
            double valor = 0;
            valor = Math.random();
            valor = valor * dado;
            valor = Math.round(valor);
            System.out.println("Tirada " + (i+1) + " de " + num + " dados de " + dado + ":");
            System.out.println(valor);
        }
    }

}