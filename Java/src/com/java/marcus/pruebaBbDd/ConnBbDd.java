package com.java.marcus.pruebaBbDd;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class DBConn {
	
	private static final String URL = "jdbc:mysql://localhost/NOMBRE_BDD";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "admin";

	public static Connection getConn() throws SQLException {
		DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
		return (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
	
}
