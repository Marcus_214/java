import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;


public class Filtro {

	public static void main(String[] args) {
		
		if (args.length==0) {
			System.out.println("Marca no especificada.");
			return;
		}
		
		String encontrar = args[0].toLowerCase();

		File fin= new File("motos.csv");
		File fout = new File(encontrar+".csv");
	
		try (	FileWriter fw = new FileWriter(fout);
				BufferedWriter bw = new BufferedWriter(fw);

				FileReader fr = new FileReader(fin);
				BufferedReader br = new BufferedReader(fr);
				) {
			
			String line;
			do {
				line = br.readLine();
				if (line!=null && line.toLowerCase().contains(encontrar)) {
					bw.write(line);
					bw.newLine();
					}
			} while (line!=null);
	
            bw.flush();
            bw.close();

		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}