package com.marcus.mavenweb01;

import java.util.ArrayList;
import java.util.List;

public class PersonaController {

    public static List<Persona> agenda = new ArrayList<Persona>();

    public static void inicializa(){

        agenda.clear();
        agenda.add(new Persona("Maria", "maria@gmailcom", 1));
        agenda.add(new Persona("Joan", "joan@gmailcom", 2));
        agenda.add(new Persona("Carla", "carla@gmailcom", 3));
        agenda.add(new Persona("Victor", "victor@gmailcom", 4));

    }

    public static Persona getById(int id_a_encontrar){

        for (Persona p : agenda){
            if (p.id == id_a_encontrar) {
                return p;
            }
        }
        return null;
        
    }

}