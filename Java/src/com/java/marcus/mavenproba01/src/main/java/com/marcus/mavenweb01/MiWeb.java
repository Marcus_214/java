package com.marcus.mavenweb01;

public class MiWeb {

    public static String miLista(String[] elementos) {
        String listaul = "<ul>";
        for (String elemento : elementos) {
            listaul = listaul + "<li>" + elemento + "</li>";
        }

        listaul = listaul + "</ul>";

        return listaul;
    }

    public static String tablaPersonas() {

        String tabla = "<table class='agenda'>";
        tabla += "<tr><th>Id</th><th>Nombre</th><th>Email</th></tr>";

        for (Persona p : PersonaController.agenda) {
            tabla += "<tr><td>" + p.id + "</td><td><a href='detalle.jsp?id=" + p.id + "'>" + p.nombre + "</a></td><td>" + p.email + "</td></tr>";
        }

        tabla += "</table>";
        return tabla;
    }

}