package com.marcus.mavenweb01;

public class Persona {

    public String nombre;
    public String email;
    public int id;

    public Persona(String nombre, String email, int id){
        this.nombre=nombre;
        this.email=email;
        this.id=id;
    }
}