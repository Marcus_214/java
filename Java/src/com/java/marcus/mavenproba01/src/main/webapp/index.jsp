<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.marcus.mavenweb01.MiWeb" %>
<%@page import="com.marcus.mavenweb01.Persona" %>
<%@page import="com.marcus.mavenweb01.PersonaController" %>

<%

    PersonaController.inicializa();
    String tablaHtml = MiWeb.tablaPersonas();

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

<h1>Lista de Agenda</h1>

<hr>
<%= tablaHtml %>
<hr>

<br>