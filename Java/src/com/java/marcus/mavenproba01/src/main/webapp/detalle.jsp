<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.marcus.mavenweb01.MiWeb" %>
<%@page import="com.marcus.mavenweb01.Persona" %>
<%@page import="com.marcus.mavenweb01.PersonaController" %>

<%

    String ids = request.getParameter("id");
    int id = Integer.parseInt(ids);
    Persona encontrada = PersonaController.getById(id);
    String datosPersona = "Persona no encontrada";
    if (encontrada != null){
        datosPersona = String.format(
            "<h2>Nombre: %s, Email: %s</h2>",
            encontrada.nombre, encontrada.email
        );
    }

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

<a href="index.jsp">Volver</a>

<h1>Detalle de contacto <%= id %></h1>
<%= datosPersona %>

<br>