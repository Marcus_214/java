package com.java.marcus;

public class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;

    Moto(String marca, String modelo){
        this.marca = marca;
        this.modelo = modelo;
    }

    @Override
    public String toString(){
        return "Moto: "+this.modelo+" ("+this.marca+")";
    }
}