package com.java.marcus;

class Test {

    public static void main(String[] args) {

        Moto m1 = new Moto("Honda", "Scoopy");
        Moto m2 = new Moto("Yamaha", "Tenere");

        System.out.println(m1);
        System.out.println(m2);

    }

}