package com.java.marcus.juisgoludopata;

public class Jugador {

    String nombre;
    int ganadas;
    int empates;
    int partidas;

    public Jugador(String nombre) {
        this.nombre = nombre;
        this.ganadas = 0;
        this.empates = 0;
        this.partidas = 0;
    }

}
