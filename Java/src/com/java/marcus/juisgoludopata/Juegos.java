package com.java.marcus.juisgoludopata;
import java.util.Random;
import java.util.Scanner;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static Maquina maquina;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);
   
    /**
     * pide nombre de jugador, 1 o 2
     * crea objeto "Jugador" y lo asigna a jugador1 o jugador2
     */
    public static void pideJugador(int numJugador){
        //pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);
        Maquina m = new Maquina();

        if (numJugador == 1) {
            Juegos.jugador1 = j;
            Juegos.maquina = m;
        }else{
            Juegos.jugador2 = j;
        }
    }

    /**
     * muestra menu de juegos y pide opción
     */
    public static void menu(){
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedro, Papel, Tijero");
        System.out.println("3: Ruleto del lodópata");
        System.out.println("*******************");
        //pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
        } while (opcion<1 || opcion>3);

        switch (opcion) {
            case 1:
                Juegos.caraCruz();
                break;
            case 2:
                Juegos.piedraPapelTijeras();
                break;
            default:
            Juegos.ruletaApuestas();
                break;
        }
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz(){
        Juegos.pideJugador(1); //solo interviene un jugador en este juego
        Juegos.partidas = 5; //partidas por defecto en este juego

        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        //bucle de partidas        
        for(int i=0; i<Juegos.partidas; i++){
            System.out.print("Cara (C) o cruz(X) ?  ");
            //pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next(); 
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
        //creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", 
                        Juegos.jugador1.nombre, 
                        Juegos.jugador1.partidas,
                        Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    public static void piedraPapelTijeras(){
        Juegos.pideJugador(1); //solo interviene un jugador en este juego
        Juegos.partidas = 5; //partidas por defecto en este juego

        System.out.println();
        System.out.println("PIEDRA, PAPEL, TIJERAS:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas a Piedra, Papel, Tijeras", Juegos.partidas);
        System.out.println();
        System.out.println();

        Random rnd = new Random(System.currentTimeMillis());
        //bucle de partidas        
        for(int i=0; i<Juegos.partidas; i++){
            System.out.print("Piedra (1), Papel (2) o Tijeras (3)");
            //pedimos apuesta aunque no se utiliza
            int apuesta = keyboard.nextInt(); 
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            int intRnd = rnd.nextInt(3)+1;
            rnd.setSeed(System.currentTimeMillis());
            // 1 = Piedra;
            // 2 = Papel;
            // 3 = Tijeras
            switch (intRnd){  //switch de resultado de la maquina
                case 1: // 1 = Piedra;
                    if (apuesta==1){
                        System.out.println();
                        System.out.println("Piedra contra piedra, empate.");
                        System.out.println();
                        Juegos.jugador1.empates++;
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.empates++;
                        Juegos.maquina.partidas++;
                    }
                    else if(apuesta==2){
                        System.out.println();
                        System.out.println("Papel contra piedra, ganas.");
                        System.out.println();
                        Juegos.jugador1.ganadas++;
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.partidas++;
                    }
                    else if (apuesta==3){
                        System.out.println();
                        System.out.println("Tijeras contra piedra, pierdes.");
                        System.out.println();
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.ganadas++;
                        Juegos.maquina.partidas++;
                    }
                    break;
                case 2: // 2 = Papel;
                    if (apuesta==1){
                        System.out.println();
                        System.out.println("Piedra contra papel, pierdes.");
                        System.out.println();
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.ganadas++;
                        Juegos.maquina.partidas++;
                    }
                    else if(apuesta==2){
                        System.out.println();
                        System.out.println("Papel contra papel, empate.");
                        System.out.println();
                        Juegos.jugador1.empates++;
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.empates++;
                        Juegos.maquina.partidas++;
                    }
                    else if (apuesta==3){
                        System.out.println();
                        System.out.println("Tijeras contra papel, ganas.");
                        System.out.println();
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.ganadas++;
                        Juegos.maquina.partidas++;
                    }
                    break;
                case 3: // 3 = Tijeras
                    if (apuesta==1){
                        System.out.println();
                        System.out.println("Piedra contra tijeras, ganas.");
                        System.out.println();
                        Juegos.jugador1.ganadas++;
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.partidas++;
                    }
                    else if(apuesta==2){
                        System.out.println();
                        System.out.println("Papel contra tijeras, pierdes.");
                        System.out.println();
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.ganadas++;
                        Juegos.maquina.partidas++;
                    }
                    else if (apuesta==3){
                        System.out.println();
                        System.out.println("Tijeras contra tijeras, empate.");
                        System.out.println();
                        Juegos.jugador1.empates++;
                        Juegos.jugador1.partidas++;
                        Juegos.maquina.ganadas++;
                        Juegos.maquina.partidas++;
                    }
                    break;
                default:
                    break;
            }
        }
        //creamos string con el resumen final de juego y lo mostramos
        String resumen1 = String.format("Jugador: %s \n%d partidas, %d ganadas, %d empatadas.", 
                        Juegos.jugador1.nombre, 
                        Juegos.jugador1.partidas,
                        Juegos.jugador1.ganadas,
                        Juegos.jugador1.empates);
                        System.out.println();
        System.out.println(resumen1);
        System.out.println();
        String resumen2 = String.format("Jugador: %s \n%d partidas, %d ganadas, %d empatadas.", 
                        Juegos.maquina.nombre, 
                        Juegos.maquina.partidas,
                        Juegos.maquina.ganadas,
                        Juegos.maquina.empates);
                        System.out.println();
        System.out.println(resumen2);
        System.out.println();
    }

    public static void ruletaApuestas(){
        Juegos.pideJugador(1); //solo interviene un jugador en este juego
    }

}
