package com.java.marcus.animales;

class Pajaro extends Animal {
    public void vuela() {
        System.out.println("Pájaro a volar");
    }
    @Override
    public int getNumeroPatas() {
        return 2;
    }
}