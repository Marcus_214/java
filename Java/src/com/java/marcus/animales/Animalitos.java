package com.java.marcus.animales;

public class Animalitos{
    public static void main(String[] args) {
        Perro toby = new Perro();

        Perro p = (Perro) toby;
        p.ladra();
        p.duerme();

        System.out.println("Numero de patas de toby: "+toby.getNumeroPatas());
        System.out.println("Numero de patas de p: "+p.getNumeroPatas());

        PerroMutante bicho = new PerroMutante();

        System.out.println("Numero de patas de bicho: "+bicho.getNumeroPatas());
    }

}